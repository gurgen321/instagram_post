@extends('layouts.app',[
"title" => "My Account"
])
@section('body')
    <div class="container">
        <form action="{{url('api/upload_media')}}" method="POST" enctype="multipart/form-data">

            <div class="form-group">
                <label for="mediaType">Post Type</label>
                <select class="form-control form-control-sm" name="media_type" id="mediaType">
                    <option value="IMAGE">Image</option>
                    <option value="VIDEO">Video</option>
                </select>
            </div>
            <div class="form-group">
                <label for="caption">Caption</label>
                <input type="text" class="form-control" name="caption" id="caption" placeholder="#TestTest">
            </div>
            <div class="form-group">
                <label for="file">Upload File</label>
                <div class="file btn btn-lg btn-primary">
                    Upload
                    <input type="file" name="file" class="file_upload" />
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

@endsection