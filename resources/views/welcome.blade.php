@extends('layouts.app', [
    "title" => 'Welcome'
])
@section('body')
    <div class="flex-center position-ref full-height">
        <div class="top-right links">
            <a href="{{url('account')}}">My Account</a>
            <a href="{{url('user/45637676592')}}">Hyper Watcher Account</a>
        </div>
        <div class="content">
            <div class="title m-b-md">
                Welcome to Instagram Feed
            </div>
        </div>
    </div>
@endsection
