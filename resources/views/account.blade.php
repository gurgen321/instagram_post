@extends('layouts.app',[
"title" => "My Account"
])
@section('body')
    <div class="container">
        <a href="{{url('add_post')}}" class="form-group btn btn-success">ADD NEW POST</a>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Post</th>
                <th scope="col">Media ID</th>
                <th scope="col">Caption</th>
                <th scope="col">Created at</th>

                <th scope="col">Enable/Disable Comment</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data as $res)
            <tr>
                <th class="align-middle" scope="row"><?=$res->id ;?></th>
                <td class="align-middle">
                    @if($res->media_type == 'VIDEO')
                        <video width="200" height="200" controls="controls" preload="none" onclick="this.play()">
                            <source src="<?=$res->file_name ;?>" >
                        </video>

                    @elseif($res->media_type == 'IMAGE')
                        <img width="200" height="200" src="<?=$res->file_name ;?>" >
                    @endif
                </td>
                <td class="align-middle"><?=$res->media_id ;?></td>
                <td class="align-middle"><?=$res->caption ;?></td>
                <td class="align-middle"><?=$res->created_at ;?></td>

                <td class="align-middle">
                    <!-- Default switch -->



                    <label class="switch">
                        <input type="checkbox" checked onchange="changeCommentAccess({{$res->media_id}})" id="commend_enable">
                        <span class="slider round"></span>
                    </label>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection