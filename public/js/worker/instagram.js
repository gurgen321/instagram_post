importScripts("../libs/Instagram.js");
// let workerServer = new Worker('our-server.js');
var instagram = new Instagram();
// workerServer.onmessage = function (e){
//
// }
// workerServer.postMessage({"aa": "11"})
self.onmessage = async (event) => {
    switch (event.data.method) {
        case 'load-index-page':
            if (Instagram.page) {
                let node = instagram.getNextPage(Instagram.page);

                await node.forEach(function (item) {
                    self.postMessage({"res": item, 'method': 'load-index-page'}, null);
                });
                if(!Instagram.page){

                    self.postMessage({ 'method': 'stop-loader-page'}, null);
                }
            }
            break;
        case 'get-post-by-shortcode':
            let postInfo = instagram.getPostByShortcode(event.data.shortcode);
            await self.postMessage({"res": postInfo, 'method': 'get-post-by-shortcode'}, null);
            break;
        case 'append-instagram-id' :
            Instagram.user_id = event.data.id;
            let node = instagram.getNextPage();
            await node.forEach(function (item) {
                self.postMessage({"res": item, 'method': 'load-index-page'}, null);
            });
            break;
    }
    self.postMessage({"end": true}, null);
};