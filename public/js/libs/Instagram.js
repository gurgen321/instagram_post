importScripts('../libs/XMLRequest.js');
class Instagram extends XMLRequest {

    static node = [];
    static page =1;
    static user_id;

    getNextPage(page = 1) {
        try {
            var data = this.to(`/api/user_data_fetch`).get({

                "page": Instagram.page
            }).json();

            if(data.data.length){
                Instagram.page++;
                Instagram.node = [];
                data.data.forEach(function (item) {

                    Instagram.node.push({
                        "shortcode": item.shortcode,
                        "display_url": item.display_url,
                        "comment_count": item.comment_count,
                        "like_count": item.like_count,
                        "post_data": JSON.parse(item.post_data)
                    });
                });
            }else{
                Instagram.page = 0;
            }

        } catch (e) {
            return [];
        }

        return Instagram.node;
    }

    getPostByShortcode(shortcode) {

        let post = this.to(`https://www.instagram.com/graphql/query/`).get({
            "query_hash": "7da1940721d75328361d772d102202a9",
            "variables": JSON.stringify({
                "shortcode": shortcode,
                "child_comment_count": "40",
                "fetch_comment_count": "40",
                "parent_comment_count": "40",
                "has_threaded_comments": true,
            })
        }).json();
        let array = {};
        array.videos = [];
        array.images = [];
        array.user = {};
        array.comments = {};
        array.post = {};
        array.user.display_url = post.data.shortcode_media.owner.profile_pic_url;
        array.user.username = post.data.shortcode_media.owner.username;
        array.user.id = post.data.shortcode_media.owner.id;
        array.user.texts = post.data.shortcode_media.edge_media_to_caption.edges;
        array.comments = post.data.shortcode_media.edge_media_to_parent_comment.edges;
        array.post.like = post.data.shortcode_media.video_view_count ? post.data.shortcode_media.video_view_count + ' <b>views</b>' : post.data.shortcode_media.edge_media_preview_like.count + ' <b>likes</b>';
        if (post.data.shortcode_media.edge_sidecar_to_children) {
            post.data.shortcode_media.edge_sidecar_to_children.edges.forEach(function (item) {
                if (item.node.video_url) {
                    array.videos.push(item.node.video_url);
                } else if (item.node.display_url) {
                    array.images.push(item.node.display_url);
                }

            })
        } else if (post.data.shortcode_media.video_url) {
            array.videos.push(post.data.shortcode_media.video_url);
        } else {
            array.images.push(post.data.shortcode_media.display_url);
        }
        return array;
    }
}