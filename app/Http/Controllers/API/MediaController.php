<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2/10/21
 * Time: 3:51 PM
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\MediaPost;
use App\Helpers\Slack;
use App\UserPost;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Facebook;
use Illuminate\Http\Request;;
use Illuminate\Support\Facades\Input;
class MediaController extends Controller
{

    public function upload(Request $request){
       $requestData=$request->all();
        $file = Input::file('file');

        $destinationPath = config('instagram_api.upload_dir') ;

//var_dump($destinationPath);die;
        $file = $request->file('file');

        if ($file != null) {
            if ($file->isValid()) {
                $extension = $file->getClientOriginalExtension(); //
                $fileName = md5(time()) . '.' . $extension; //

                $file->move($destinationPath, $fileName);
            } else {
                return array('error' => 'upload_error');
            }
        }
        $fb = new Facebook([
            'app_id' => config('instagram_api.app_id'),
            'app_secret' => config('instagram_api.app_secret'),
            'default_graph_version' => 'v9.0',
            //'default_access_token' => '{access-token}', // optional
        ]);


        try {
            $accessToken=$this->refreshToken();
            // Returns a `FacebookFacebookResponse` object
            if($requestData['media_type'] == 'IMAGE'){
                $response = $fb->post(
                    '/'.config("instagram_api.ig_user_id").'/media',
                    array (
                        'image_url' => url('uploads/'.$fileName),
                        'caption' => $requestData['caption']
                    ),
                    $accessToken
                );

            }else{
                try {
                    // Returns a `FacebookFacebookResponse` object
                    $response = $fb->post(
                        '/17841446098030649/media',
                        array (
                            'media_type' => 'VIDEO',
                            'video_url' => 'https://instagram.fevn2-1.fna.fbcdn.net/v/t50.2886-16/148988823_234429041626705_3598029833916925851_n.mp4?_nc_ht=instagram.fevn2-1.fna.fbcdn.net&_nc_cat=101&_nc_ohc=qrq1Iarr7_MAX_S_10D&oe=6029057B&oh=2ad9141c049e555b15afcfd881f0d2d8',
                            'caption' => $requestData['caption'],
                            '_nc_cat' => '101',
                            '_nc_ohc' => 'qrq1Iarr7_MAX_S_10D',
                            'oe' => '6029057B',
                            'oh' => '2ad9141c049e555b15afcfd881f0d2d8'
                        ),
                        $accessToken
                    );
                } catch(FacebookExceptionsFacebookResponseException $e) {
                    echo 'Graph returned an error: ' . $e->getMessage();
                    exit;
                } catch(FacebookExceptionsFacebookSDKException $e) {
                    echo 'Facebook SDK returned an error: ' . $e->getMessage();
                    exit;
                }


            }
        } catch(FacebookResponseException $e) {
            return 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(FacebookResponseException $e) {
            return 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        $graphNode = $response->getDecodedBody();

        $result = $this->mediaPush($graphNode);
        try{

            $instagramPostData=$this->postData($result['id']);
            $new_post = new MediaPost();
            $new_post->media_type = $instagramPostData['data']['media_type'];
            $new_post->media_id = $result['id'];
            $new_post->caption = isset($instagramPostData['data']['caption']) ? $instagramPostData['data']['caption'] : null;
            $new_post->file_name = $instagramPostData['data']['media_url'];
            $new_post->save();
            $userName=config('instagram_api.username');
            Slack::slack_post_message($userName. ' added a new post . Go to https://instagram.com/'.$userName);
        }catch (\Exception $e){
            return 'Error saving media '. $e->getMessage();
        }
        return redirect('account');

    }
    public function mediaPush($result){
        $fb = new Facebook([
            'app_id' => config('instagram_api.app_id'),
            'app_secret' => config('instagram_api.app_secret'),
            'default_graph_version' => 'v9.0',
            //'default_access_token' => '{access-token}', // optional
        ]);

        $responseInProgress=$this->checkStatus($result['id']);
        if($responseInProgress['data']['status_code'] == 'IN_PROGRESS'){
            $this->mediaPush($result);
        }
        try {
            // Returns a `FacebookFacebookResponse` object
            $response = $fb->post(
                '/'.config("instagram_api.ig_user_id").'/media_publish',
                array (
                    'creation_id' => $result['id']
                ),
                config("instagram_api.access_token")
            );
        } catch(FacebookResponseException $e) {


            return 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(FacebookResponseException $e) {
            return 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        return $response->getDecodedBody();
    }
    public function refreshToken(){
        $curl = curl_init();
        $clientId=config('instagram_api.app_id');
        $clientSecret=config('instagram_api.app_secret');
        $accessToken=config("instagram_api.access_token");
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=$clientId&client_secret=$clientSecret&fb_exchange_token=$accessToken",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => array('a' => 'a'),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $response=json_decode($response,true);
        config("instagram_api.access_token",$response['access_token']);
        return $response['access_token'];

    }
    public function enableComments(Request $request){
        $requestData=$request->all();
        $fb = new Facebook([
            'app_id' => config('instagram_api.app_id'),
            'app_secret' => config('instagram_api.app_secret'),
            'default_graph_version' => 'v9.0',
            //'default_access_token' => '{access-token}', // optional
        ]);


        try {
            $accessToken=$this->refreshToken();
            // Returns a `FacebookFacebookResponse` object
            $response = $fb->post(
                '/'.$requestData['media_id'],
                array (
                    'comment_enabled' => $requestData['enable']
                ),
                $accessToken
            );
        } catch(FacebookResponseException $e) {
            return 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(FacebookResponseException $e) {
            return 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        return $response->getDecodedBody();
    }

    public function allPosts(){

        $fb = new Facebook([
            'app_id' => config('instagram_api.app_id'),
            'app_secret' => config('instagram_api.app_secret'),
            'default_graph_version' => 'v9.0',
            //'default_access_token' => '{access-token}', // optional
        ]);


        try {
            $accessToken=$this->refreshToken();
            // Returns a `FacebookFacebookResponse` object
            $response = $fb->get(
                '/'.config("instagram_api.ig_user_id").'/media',
                $accessToken
            );
        } catch(FacebookResponseException $e) {
            return 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(FacebookResponseException $e) {
            return 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        $data=$response->getDecodedBody();
        return array('success'=>true,'data'=>$data['data']);
    }
    public function postData($id){

        $fb = new Facebook([
            'app_id' => config('instagram_api.app_id'),
            'app_secret' => config('instagram_api.app_secret'),
            'default_graph_version' => 'v9.0',
            //'default_access_token' => '{access-token}', // optional
        ]);


        try {

            // Returns a `FacebookFacebookResponse` object
            $response = $fb->get(
                '/'.$id.'?fields=caption,media_url,media_type,timestamp',
                config("instagram_api.access_token")
            );
        } catch(FacebookResponseException $e) {
            return 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(FacebookResponseException $e) {
            return 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        return array('success'=>true,'data'=>$response->getDecodedBody());
    }
    public function checkStatus($id){
        $fb = new Facebook([
            'app_id' => config('instagram_api.app_id'),
            'app_secret' => config('instagram_api.app_secret'),
            'default_graph_version' => 'v9.0',
            //'default_access_token' => '{access-token}', // optional
        ]);


        try {

            // Returns a `FacebookFacebookResponse` object
            $response = $fb->get(
                '/'.$id.'?fields=status_code',
                config("instagram_api.access_token")
            );
        } catch(FacebookResponseException $e) {
            return 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(FacebookResponseException $e) {
            return 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        return array('success'=>true,'data'=>$response->getDecodedBody());
    }
    public function fetchUserData(){
        return UserPost::paginate(12);
    }
}