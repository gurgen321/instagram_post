<?php

namespace App\Http\Controllers;

use App\MediaPost;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function index()
    {
        $data=MediaPost::all();
        return view('account',compact('data'));
    }
    public function addPost(){
        return view('add_post');
    }
}
