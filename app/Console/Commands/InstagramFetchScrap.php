<?php

namespace App\Console\Commands;

use App\Helpers\InstagramFetch;
use Illuminate\Console\Command;

class InstagramFetchScrap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'instagram:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch data from hyperwatcher account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * eturn mixed
     */
    public function handle(InstagramFetch $fetch)
    {
        echo "start";
        $fetch->getNextPage('45637676592');
        echo "end";

    }
}
