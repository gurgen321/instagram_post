<?php

namespace App\Console\Commands;

use App\Http\Controllers\API\MediaController;
use App\MediaPost;
use App\Helpers\Slack;
use Illuminate\Console\Command;

class InstagramFetchData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'instagram:posts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch new posts from my account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $media=new MediaController();
       $allData=$media->allPosts();
        if($allData['success']){
            $datas=$allData['data'];
            foreach ($datas as $data){
                foreach ($data as $d){

                   $ids[]=$d;
                }
            }
            //check if post is deleted from instagram
            $myPosts = MediaPost::select('media_id')->get();
            foreach ($myPosts as $myPost){
                if(!in_array($myPost['media_id'],$ids)){
                    MediaPost::where('media_id',$myPost['media_id'])->delete();
                }
            }
            //update with new posts
            for($i=0;$i<count($ids);$i++){
                if(MediaPost::select('id')->where('media_id',$ids[$i])->count() === 0){
                    echo $ids[$i];
                    $instagramPostData=$media->postData($ids[$i]);
                    $created_date=date('Y-m-d h:m:s',strtotime($instagramPostData['data']['timestamp']));

                    $new_post = new MediaPost();
                    $new_post->media_type = $instagramPostData['data']['media_type'];
                    $new_post->media_id = $ids[$i];
                    $new_post->caption = isset($instagramPostData['data']['caption']) ? $instagramPostData['data']['caption'] : null;
                    $new_post->file_name = $instagramPostData['data']['media_url'];
                    $new_post->created_at = $created_date;
                    $new_post->save();
                    echo  ' added ';
                    $userName=config('instagram_api.username');
                    Slack::slack_post_message( $userName. ' added a new post . Go to https://instagram.com/'.$userName);
                    //here we can sent slack notification
                }
            }

        }
    }
}
