<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2/11/21
 * Time: 1:21 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class MediaPost extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'my_posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','media_type','media_id','caption','file_name'];




}