<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2/12/21
 * Time: 11:27 AM
 */

namespace App\Helpers;


use App\User;
use App\UserPost;

class InstagramFetch
{
    private $next_page = '';

    private $arrayData = array();



    public function getNextPage($user_id = '45637676592', $next_page = '')
    {
        $curl = curl_init();

        $url = "http://www.instagram.com/graphql/query/?query_id=17888483320059182&id={$user_id}&first=20&after={$next_page}";
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_PROXY => "http://scraperapi:0f0c22d4294281c35e409938421822c5@proxy-server.scraperapi.com:8001",
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                'Accept: application/json',
                'Cache-Control: no-cache',
                'Referer: https://www.instagram.com',

            ),
        ));
        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        $error=curl_error($curl);
        curl_close($curl);
        if ($info['http_code'] != 200) {
            //throw exception
            dd($response);

        } else {
            $response = json_decode($response, true);

            $this->next_page = $response['data']['user']['edge_owner_to_timeline_media']['page_info']['end_cursor'] ?? '';

            foreach ($response['data']['user']['edge_owner_to_timeline_media']['edges'] as $item) {
                $this->arrayData[] = [
                    "shortcode" => $item['node']['shortcode'],
                    "display_url" => $item['node']['display_url'],
                    "comment_count" => $item['node']['edge_media_to_comment']['count'],
                    "like_count" => $item['node']['edge_media_preview_like']['count'],
                    "created_at" => $item['node']['taken_at_timestamp'],
                    "post_data" => [
                        "like_count" => !empty($item['node']['video_view_count']) ? $item['node']['video_view_count'] : $item['node']['edge_media_preview_like']['count'],
                        "like_icon" => !empty($item['node']['video_view_count']) ? 'fa fa-play' : 'fa fa-heart',
                        "post_icon" => !empty($item['node']['video_view_count']) ? 'fa fa-video-camera' : 'fa fa-clone',
                    ]
                ];
            }

            foreach ($this->arrayData as $index => $item) {
                $ifexistsPost = UserPost::select('id')->where('shortcode',$item['shortcode'])->first();

                if (!$ifexistsPost) {
                    $this->getPostByShortcode($item['shortcode'], $item, $user_id);

                } else {
//                    return false;
                }
            }
            $this->arrayData = array();
            if (strlen($this->next_page)) {
                return $this->getNextPage($user_id, $this->next_page);
            }
            return false;
        }
    }

    private function getPostByShortcode($shortcode, $item, $user_id)
    {
        $curl = curl_init();

        $url = "http://www.instagram.com/graphql/query/?query_hash=7da1940721d75328361d772d102202a9&variables=%7B%22shortcode%22%3A%22{$shortcode}%22%2C%22child_comment_count%22%3A%2210%22%2C%22fetch_comment_count%22%3A%2210%22%2C%22parent_comment_count%22%3A%2210%22%2C%22has_threaded_comments%22%3Atrue%7D";
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_PROXY => "http://scraperapi:0f0c22d4294281c35e409938421822c5@proxy-server.scraperapi.com:8001",
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                'Accept: application/json',
                'Cache-Control: no-cache',
                'Referer: https://www.instagram.com'
            ),
        ));
        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        if ($info['http_code'] != 200) {
            //throw exception
            dd($response);
            return false;
        } else {
            $response = json_decode($response, true);
            $array = [];
            $array['videos'] = [];
            $array['images'] = [];
            $array['user'] = [];
            $array['comments'] = [];
            $array['post'] = [];
            $array['user']['display_url'] = $response['data']['shortcode_media']['owner']['profile_pic_url'];
            $array['user']['username'] = $response['data']['shortcode_media']['owner']['username'];
            $array['user']['id'] = $response['data']['shortcode_media']['owner']['id'];
            $array['user']['taken_at_timestamp'] = $response['data']['shortcode_media']['taken_at_timestamp'];
            $array['user']['texts'] = empty($response['data']['shortcode_media']['edge_media_to_caption']['edges']) ? '' : $response['data']['shortcode_media']['edge_media_to_caption']['edges'][0]['node']['text'];
            $array['comments'] = $response['data']['shortcode_media']['edge_media_to_parent_comment']['edges'];
            $array['post']['like'] = !empty($response['data']['shortcode_media']['video_view_count']) ? $response['data']['shortcode_media']['video_view_count'] . ' views' : $response['data']['shortcode_media']['edge_media_preview_like']['count'] . ' likes';

            if (isset($response['data']['shortcode_media']['edge_sidecar_to_children'])) {
                foreach ($response['data']['shortcode_media']['edge_sidecar_to_children']['edges'] as $item) {
                    if (isset($item['node']['video_url'])) {
                        $array['videos'][] = $item['node']['video_url'];
                    } else if (isset($item['node']['display_url'])) {
                        $array['images'][] = $item['node']['display_url'];
                    }
                }
            } else if (isset($response['data']['shortcode_media']['video_url'])) {
                $array['videos'][] = $response['data']['shortcode_media']['video_url'];
            } else {
                $array['images'][] = $response['data']['shortcode_media']['display_url'];
            }


            $created_date=date('Y-m-d h:m:s',$item['created_at']);
            $feed = new UserPost();
            $feed->shortcode = $shortcode;
            $feed->display_url = $item['display_url'];
            $feed->comment_count = $item['comment_count'];
            $feed->like_count = $item['like_count'];
            $feed->post_count = count($array['videos']) + count($array['images']);
            $feed->created_at = $created_date;
            $feed->post_content = json_encode($array);
            $feed->post_data = json_encode($item['post_data']);
            $feed->save();
            return true;
        }

    }
}