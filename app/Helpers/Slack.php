<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2/11/21
 * Time: 7:16 PM
 */

namespace App\Helpers;


class Slack
{

    public static function slack_post_message($text = "") {
        $params = array(
            "text" => $text
        );
        return self::postData("https://hooks.slack.com/services/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", json_encode($params),array("Content-Type: application/json"));
    }


    private static function postData($url, $postFields, $headers = []) {

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSLVERSION,"CURL_SSLVERSION_SSLv3");
        $response = curl_exec($ch);
        $errno = curl_errno($ch);
        $error = curl_error($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        if($errno) {
            //throw exception
            //for example
            return false;
        } else {
           //return result
            //example
            return true;
        }
    }
}