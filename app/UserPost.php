<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPost extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['shortcode','display_url','comment_count','like_count','post_count','post_content','post_data','created_at'];
}
